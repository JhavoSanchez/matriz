<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MatrizController extends Controller
{
	public function index(Request $request)
	{
		$matriz = $request->input();

		$rpta = [
			"valor" => 1,
			"msg" => null
		];

        $rpta = $this->validarSiMatrizEsBidimensional($rpta, $matriz);
        $rpta = $this->validarTipoArray($rpta, $matriz);
		$rpta = $this->validarElementoDeArrayPadre($rpta, $matriz);
		$rpta = $this->girarSentidoAntihorario($rpta, $matriz);

		return response()->json($rpta);
	}

    /**
     * Validamos SI la matriz es bidimensional
     *
     * @author Harvey Sanchez <havo.sanchez@gmail.com>
     * @since 12-12-2019
     * @version 1.0
     * @param array $rpta Respuesta de validaciones
     * @param array $matriz Colección padre o hijo
     * @return Array de resultado de validación
     */
    private function validarSiMatrizEsBidimensional($rpta, $matriz)
    {
        // Validamos SI todo va bien
        if ($rpta["valor"] == 1) {
            if (count($matriz) == count($matriz, COUNT_RECURSIVE)) {
                $rpta["valor"] = 0;
                $rpta["msg"] = "La matriz debe ser bidimensional.";
            }
        }

        return $rpta;
    }

    /**
     * Validamos SI el valor pasado es de tipo array
     *
     * @author Harvey Sanchez <havo.sanchez@gmail.com>
     * @since 12-12-2019
     * @version 1.0
     * @param array $rpta Respuesta de validaciones
     * @param array $matriz Colección padre o hijo
     * @return Array de resultado de validación
     */
    private function validarTipoArray($rpta, $coleccion)
    {
        // Validamos SI todo va bien
        if ($rpta["valor"] == 1) {
            if (!is_array($coleccion)) {
                $rpta["valor"] = 0;
                $rpta["msg"] = "La matriz y/o algún elemento no es de tipo array.";
            }
        }

        return $rpta;
    }

    /**
     * Validamos los elementos de la matriz
     *
     * @author Harvey Sanchez <havo.sanchez@gmail.com>
     * @since 12-12-2019
     * @version 1.0
     * @param array $rpta Respuesta de validaciones
     * @param array $coleccion Colección proporcionada por el usuario
     * @return Array de resultado de validación
     */
    private function validarElementoDeArrayPadre($rpta, $coleccion)
    {
    	// Validamos SI todo va bien
    	if ($rpta["valor"] == 1) {
    		foreach ($coleccion as $key => $elemento) {
    			$rpta = $this->validarTipoArray($rpta, $elemento);
    			$rpta = $this->validarLongitudElemento($rpta, $coleccion, $key);
    			$rpta = $this->validarElementoDeArrayHijo($rpta, $elemento);
    			if ($rpta["valor"] == 0) break;
    		}
    	}

    	return $rpta;
    }

    /**
     * Validamos SI la longitud de los array hijos son iguales a los array padre
     *
     * @author Harvey Sanchez <havo.sanchez@gmail.com>
     * @since 12-12-2019
     * @version 1.0
     * @param array $rpta Respuesta de validaciones
     * @param array $matriz Matriz proporcionada por el usuario
     * @param int $key Índice del elemento de la matriz
     * @return Array de resultado de validación
     */
    private function validarLongitudElemento($rpta, $matriz, $key)
    {
    	// Validamos SI todo va bien
    	if ($rpta["valor"] == 1) {
    		$longitudMatriz = count($matriz);
    		$longitudElementoMatriz = count($matriz[$key]);

    		if ($longitudMatriz != $longitudElementoMatriz) {
				$rpta["valor"] = 0;
				$rpta["msg"] = "La matriz ingresada no tiene la misma dimensión.";
    		}
    	}

    	return $rpta;
    }

    /**
     * Validamos los elementos de los array hijos
     *
     * @author Harvey Sanchez <havo.sanchez@gmail.com>
     * @since 12-12-2019
     * @version 1.0
     * @param array $rpta Respuesta de validaciones
     * @param array $elemento Colección hijo respecto a la matriz
     * @return Array de resultado de validación
     */
    private function validarElementoDeArrayHijo($rpta, $coleccion)
    {
    	// Validamos SI todo va bien
    	if ($rpta["valor"] == 1) {
    		foreach ($coleccion as $key => $elemento) {
    			$rpta = $this->validarTipoNumerico($rpta, $elemento);
    			if ($rpta["valor"] == 0) break;
    		}
    	}

    	return $rpta;
    }

    /**
     * Validamos SI el elemento del array hijo es de tipo numérico
     *
     * @author Harvey Sanchez <havo.sanchez@gmail.com>
     * @since 12-12-2019
     * @version 1.0
     * @param array $rpta Respuesta de validaciones
     * @param array $matriz Colección padre o hijo
     * @return Array de resultado de validación
     */
    private function validarTipoNumerico($rpta, $elemento)
    {
    	// Validamos SI todo va bien
    	if ($rpta["valor"] == 1) {
	    	if (!is_numeric($elemento)) {
	    		$rpta["valor"] = 0;
	    		$rpta["msg"] = "Se encontraron valores que no son de tipo numérico.";
	    	}
    	}

    	return $rpta;
    }

    /**
     * Giramos una matriz en sentido antihorario
     *
     * @author Harvey Sanchez <havo.sanchez@gmail.com>
     * @since 12-12-2019
     * @version 1.0
     * @param array $rpta Respuesta de validación
     * @param array $coleccion Colección proporcionada por el usuario
     * @return Array de resultado del giro
     */
    private function girarSentidoAntihorario($rpta, $matriz)
    {
    	// Validamos SI todo va bien
    	if ($rpta["valor"] == 1) {
    		$nuevaMatriz = [];
    		$longitudMatriz = count($matriz);

    		for ($i = 0; $i < $longitudMatriz; $i++) {
    			$indice = $longitudMatriz - ($i + 1);
    			$nuevaMatriz[$i] = array_column($matriz, $indice);
    		}

    		$rpta["msg"] = "La matriz fue girada satisfactoriamente en sentido anti-horario.";
    		$rpta["matriz"] = [
    			"input" => $matriz,
    			"output" => $nuevaMatriz
    		];
    	}

    	return $rpta;
    }
}
